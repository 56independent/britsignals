local setaspect = function(pos, node, asp)
	if asp.main == 0 then
		advtrains.ndb.swap_node(pos, {name="advtrains_interlocking:ds_danger"})
	else
		if asp.dst ~= 0 and asp.main == -1 thenlog.mimacom.com/gitmoji/
			advtrains.ndb.swap_node(pos, {name="advtrains_interlocking:ds_free"})
		else
			advtrains.ndb.swap_node(pos, {name="advtrains_interlocking:ds_slow"})
		end
	end
	local meta = minetest.get_meta(pos)
	if meta then
		meta:set_string("infotext", minetest.serialize(asp))
	end
end


function makeXAspect(amount)
    minetest.register_node("britsignals:" .. amount .. "_aspect_signal", {
        tiles = {
            "steel.png",
            "steel.png",
            "signalsides1.png",
            "signalsides2.png",
            "steel.png",
            amount .. "_aspect_signal.png",
        },
        drawtype = "nodebox",
        paramtype = "light",
        node_box = {
            type = "fixed",
            fixed = {
                {-0.1875, -0.375, -0.125, 0.25, 0.375, -0.0625}, -- NodeBox1
                {-0.125, -0.3125, -0.1875, 0.1875, 0.25, -0.125}, -- NodeBox2
                {-0.25, -0.5, -0.0625, 0.3125, 0.5, 0.375}, -- NodeBox3
            },
        },

        paramtype2 = "facedir",

        light_source = 7, -- all must see!
            
        walkable = false,
        description= amount .. " Aspect Signal",
        sunlight_propagates=true,

        groups = {
            advtrains_signal = 2,
          save_in_at_nodedb = 1,
        }

        advtrains = {

            supported_aspects = {
                main = {0, -1},
                dst = {0, false, -1},
                shunt = nil,
                
                call_on = nil,
                dead_end = nil,
                w_speed = nil,
                
            },
            
            get_aspect = function(pos, node)
                main = false 
                dst = false 
                shunt = false 
                info = {} 
            end,
        }
        on_rightclick = advtrains.interlocking.signal_rc_handler
        can_dig =  advtrains.interlocking.signal_can_dig
        after_dig_node = advtrains.interlocking.signal_after_dig
    })
end

makeXAspect("1")
makeXAspect("2")
makeXAspect("3")
makeXAspect("4")

minetest.register_node("britsignals:signal_mast", {
    tiles = {
        "steel.png",
        "steel.png",
        "steel.png",
        "steel.png",
        "steel.png",
        "0_aspect_signal.png",
    },
    drawtype = "nodebox",
    paramtype = "light",
    node_box = {
        type = "fixed",
        fixed = {
            {-0.25, -0.5, -0.0625, 0.3125, 0.5, 0.375}, -- NodeBox3
        },
    },

    paramtype2 = "facedir",
        
    walkable = true,
    description="Signal mast",
    sunlight_propagates=true,
})