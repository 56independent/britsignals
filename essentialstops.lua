-- Less full version of stops.lua
--[[
Essentials are defined by:
    * Black and Blue backs
    * Goes up to 10 cars, not 30
    * Crossings are only blue and black
]]--

dofile(minetest.get_modpath("britsignals") .. "/makesign.lua")

makeStop("white_s", "Stop white of thing? cars")
makeStop("black_s", "Stop black of thing? cars")
makeStop("blue_s", "Stop blue of thing? cars")
makeStop("white_1", "Stop white of 1 cars")
makeStop("black_1", "Stop black of 1 cars")
makeStop("blue_1", "Stop blue of 1 cars")
makeStop("white_2", "Stop white of 2 cars")
makeStop("black_2", "Stop black of 2 cars")
makeStop("blue_2", "Stop blue of 2 cars")
makeStop("white_3", "Stop white of 3 cars")
makeStop("black_3", "Stop black of 3 cars")
makeStop("blue_3", "Stop blue of 3 cars")
makeStop("white_4", "Stop white of 4 cars")
makeStop("black_4", "Stop black of 4 cars")
makeStop("blue_4", "Stop blue of 4 cars")
makeStop("white_5", "Stop white of 5 cars")
makeStop("black_5", "Stop black of 5 cars")
makeStop("blue_5", "Stop blue of 5 cars")
makeStop("white_6", "Stop white of 6 cars")
makeStop("black_6", "Stop black of 6 cars")
makeStop("blue_6", "Stop blue of 6 cars")
makeStop("white_7", "Stop white of 7 cars")
makeStop("black_7", "Stop black of 7 cars")
makeStop("blue_7", "Stop blue of 7 cars")
makeStop("white_8", "Stop white of 8 cars")
makeStop("black_8", "Stop black of 8 cars")
makeStop("blue_8", "Stop blue of 8 cars")
makeStop("white_9", "Stop white of 9 cars")
makeStop("black_9", "Stop black of 9 cars")
makeStop("blue_9", "Stop blue of 9 cars")
makeStop("white_10", "Stop white of 10 cars")
makeStop("black_10", "Stop black of 10 cars")
makeStop("blue_10", "Stop blue of 10 cars")


makeStop("black_1_1", "Stop black of 1 cars")
makeStop("blue_1_1", "Stop blue of 1 cars")

makeStop("black_1_2", "Stop black of 1 cars")
makeStop("blue_1_2", "Stop blue of 1 cars")

makeStop("black_1_3", "Stop black of 1 cars")
makeStop("blue_1_3", "Stop blue of 1 cars")

makeStop("black_1_4", "Stop black of 1 cars")
makeStop("blue_1_4", "Stop blue of 1 cars")

makeStop("black_1_5", "Stop black of 1 cars")
makeStop("blue_1_5", "Stop blue of 1 cars")

makeStop("black_1_6", "Stop black of 1 cars")
makeStop("blue_1_6", "Stop blue of 1 cars")

makeStop("black_1_7", "Stop black of 1 cars")
makeStop("blue_1_7", "Stop blue of 1 cars")

makeStop("black_1_8", "Stop black of 1 cars")
makeStop("blue_1_8", "Stop blue of 1 cars")

makeStop("black_1_9", "Stop black of 1 cars")
makeStop("blue_1_9", "Stop blue of 1 cars")

makeStop("black_1_10", "Stop black of 1 cars")
makeStop("blue_1_10", "Stop blue of 1 cars")

makeStop("black_2_1", "Stop black of 2 cars")
makeStop("blue_2_1", "Stop blue of 2 cars")

makeStop("black_3_1", "Stop black of 3 cars")
makeStop("blue_3_1", "Stop blue of 3 cars")

makeStop("black_3_2", "Stop black of 3 cars")
makeStop("blue_3_2", "Stop blue of 3 cars")

makeStop("black_4_1", "Stop black of 4 cars")
makeStop("blue_4_1", "Stop blue of 4 cars")

makeStop("black_4_2", "Stop black of 4 cars")
makeStop("blue_4_2", "Stop blue of 4 cars")

makeStop("black_4_3", "Stop black of 4 cars")
makeStop("blue_4_3", "Stop blue of 4 cars")

makeStop("black_4_5", "Stop black of 4 cars")
makeStop("blue_4_5", "Stop blue of 4 cars")

makeStop("black_5_1", "Stop black of 5 cars")
makeStop("blue_5_1", "Stop blue of 5 cars")

makeStop("black_5_2", "Stop black of 5 cars")
makeStop("blue_5_2", "Stop blue of 5 cars")

makeStop("black_5_3", "Stop black of 5 cars")
makeStop("blue_5_3", "Stop blue of 5 cars")

makeStop("black_5_4", "Stop black of 5 cars")
makeStop("blue_5_4", "Stop blue of 5 cars")

makeStop("black_5_6", "Stop black of 5 cars")
makeStop("blue_5_6", "Stop blue of 5 cars")

makeStop("black_6_1", "Stop black of 6 cars")
makeStop("blue_6_1", "Stop blue of 6 cars")

makeStop("black_6_2", "Stop black of 6 cars")
makeStop("blue_6_2", "Stop blue of 6 cars")

makeStop("black_6_3", "Stop black of 6 cars")
makeStop("blue_6_3", "Stop blue of 6 cars")

makeStop("black_6_4", "Stop black of 6 cars")
makeStop("blue_6_4", "Stop blue of 6 cars")

makeStop("black_6_5", "Stop black of 6 cars")
makeStop("blue_6_5", "Stop blue of 6 cars")

makeStop("black_7_1", "Stop black of 7 cars")
makeStop("blue_7_1", "Stop blue of 7 cars")

makeStop("black_7_2", "Stop black of 7 cars")
makeStop("blue_7_2", "Stop blue of 7 cars")

makeStop("black_7_3", "Stop black of 7 cars")
makeStop("blue_7_3", "Stop blue of 7 cars")

makeStop("black_7_4", "Stop black of 7 cars")
makeStop("blue_7_4", "Stop blue of 7 cars")

makeStop("black_7_5", "Stop black of 7 cars")
makeStop("blue_7_5", "Stop blue of 7 cars")

makeStop("black_7_6", "Stop black of 7 cars")
makeStop("blue_7_6", "Stop blue of 7 cars")

makeStop("black_8_1", "Stop black of 8 cars")
makeStop("blue_8_1", "Stop blue of 8 cars")

makeStop("black_8_2", "Stop black of 8 cars")
makeStop("blue_8_2", "Stop blue of 8 cars")

makeStop("black_8_3", "Stop black of 8 cars")
makeStop("blue_8_3", "Stop blue of 8 cars")

makeStop("black_8_4", "Stop black of 8 cars")
makeStop("blue_8_4", "Stop blue of 8 cars")

makeStop("black_8_5", "Stop black of 8 cars")
makeStop("blue_8_5", "Stop blue of 8 cars")

makeStop("black_8_6", "Stop black of 8 cars")
makeStop("blue_8_6", "Stop blue of 8 cars")

makeStop("black_8_7", "Stop black of 8 cars")
makeStop("blue_8_7", "Stop blue of 8 cars")

makeStop("black_9_1", "Stop black of 9 cars")
makeStop("blue_9_1", "Stop blue of 9 cars")

makeStop("black_9_2", "Stop black of 9 cars")
makeStop("blue_9_2", "Stop blue of 9 cars")

makeStop("black_9_3", "Stop black of 9 cars")
makeStop("blue_9_3", "Stop blue of 9 cars")

makeStop("black_9_4", "Stop black of 9 cars")
makeStop("blue_9_4", "Stop blue of 9 cars")

makeStop("black_9_5", "Stop black of 9 cars")
makeStop("blue_9_5", "Stop blue of 9 cars")

makeStop("black_9_6", "Stop black of 9 cars")
makeStop("blue_9_6", "Stop blue of 9 cars")

makeStop("black_9_7", "Stop black of 9 cars")
makeStop("blue_9_7", "Stop blue of 9 cars")

makeStop("black_9_8", "Stop black of 9 cars")
makeStop("blue_9_8", "Stop blue of 9 cars")

makeStop("black_10_1", "Stop black of 10 cars")
makeStop("blue_10_1", "Stop blue of 10 cars")

makeStop("black_10_2", "Stop black of 10 cars")
makeStop("blue_10_2", "Stop blue of 10 cars")

makeStop("black_10_3", "Stop black of 10 cars")
makeStop("blue_10_3", "Stop blue of 10 cars")

makeStop("black_10_4", "Stop black of 10 cars")
makeStop("blue_10_4", "Stop blue of 10 cars")

makeStop("black_10_5", "Stop black of 10 cars")
makeStop("blue_10_5", "Stop blue of 10 cars")

makeStop("black_10_6", "Stop black of 10 cars")
makeStop("blue_10_6", "Stop blue of 10 cars")

makeStop("black_10_7", "Stop black of 10 cars")
makeStop("blue_10_7", "Stop blue of 10 cars")

makeStop("black_10_8", "Stop black of 10 cars")
makeStop("blue_10_8", "Stop blue of 10 cars")

makeStop("black_10_9", "Stop black of 10 cars")
makeStop("blue_10_9", "Stop blue of 10 cars")