# Introduction
This mod adds british-inspired signals to minetest.

# Nodes
"Signals" refer to any device designed to give information to drivers. They are as referenced from a [http://railsigns.uk](website). The static signals are just signs: you have to build infrastructure around them to make them fit in.

Masts hold signals and signs, and ballast allows for various conditions of ballast.

Only the masts and electronic information has been made yet
 
 
## Static Signals
### Electronic Information
These signals give information on electronics around the railway line. They are as taken from a [http://railsigns.uk/sect18page1.html](website)

* Dead Track - Lit automatically if electricity turns off. The driver should call the signalman if it is lit.
* Flood - Lit if there is water on the tracks.
* Electric End - Placed where electric power supply ends.
* Live Rails - For when there is a third rail system in operation.
* Neutral Section - Announces the start of a neutral section, where there is no power for trains.
* Neutral Section Warning - Announces that a neutral section is coming.
* Overhead Wires - To warn of wires being overhead.
* Blue Signs - Tells a specific locomotive telling what to do to the power sources. 

<!--## Speed Limits
These signals are based on british speed limit signs, and go up in denominations based on ks signals.-->

## Station Signs
Stations need signs to warn passangers and civillians around the railway of things. These include:

* Horn signs - Tells the driver to honk their horn, like when coming across a level crossing.
* Tresspassing - Warns civilians of cost of being on the railway lines.
* Turbulence - Warns civillians of the turbulence of trains which skip the station.
* Kill sign - Warns people that trains kill at 6 m/s and to stay off the track
* Kill sign simple - Warns people to stay on the platform because trains kill.
* Height - Warns that there is a great and deadly height before the players. 
* Queues - Warns drivers that there are queues likely, either from a poorly-designed station or interlocking errors. 
* Silent - Warns people trains are silet killers.

### Stops
**Warning: There are an extreme amount of nodes (359) established with this file. To enable this mod and risk you from hitting the node limit, uncomment `--dofile(minetest.get_modpath("britsignals") .. "/stops.lua")`, by removing `--` at the beggining of the line. This does require that you comment out the essentialstops.lua file to prevent node conflicts.**

These tell the train where to stop at a station. These do not control the trains; use luaatc and set_route for that:

```lua
if event.type == "train" then 
    length = "train_length"
    set_rc(length)
end
```

Set the station tracks to `RC x`, where x is the car stop.

An "s" means the stop of all trains, and a number repersents the place of a train of that length to stop, for "optimum train posistioning"

Stops have these various information:

* 1-20 car stops
* Modular signs, allowing bigger stop signs, such as "stop 4-6", using above selection with dash-number signs.
* Wildcards - Match all cars

of these backgrounds:

* Blue
* Black
* White with red text
## Dynamic Signals
Dynamic signals are signals designed to be dynamic.

* Manchester Tram Signal - A horizontal line means danger, a vertical line means clear. The line of lights may be doglegged. This means that you are taking that specific route over an intersection.
* Route Feather - An addon, this signal allows the signal below to communicate the route
* 4-aspect - Based on 4-aspect British signals, distant signalling is not implemented yet, but will be when advtrains intrigates distant signalling
* 8-aspect - Using the 4-aspect signal, this adds more aspects. The aspects are (in order of danger):
 * R
 * R-Y
 * R-Y-Y
 * Y
 * Y-Y
 * G-Y
 * G-Y-Y
 * G
* 3-aspect - Slightly shorter and cheaper then the 4-aspect signals, these are quite simple.
* 2-aspect - The simplest of signals, comes in two forms - R-G and R-Y, with R-Y being a distant signal (purely decoration).
* Black Bag - Covering a signal with a delicate white cross, this takes out signals not in use. 
* Double-face - Has a constantly red back and a variable front. The red back is just decoration.
* Shunting - A small signal off to the side. 

## Track Elements
### Ballast and track decorations
Ballast is important for any railway. It holds the tracks and allows drainage. Most of the ballast is made through a combination of gravel, stone, and cobblestone's textures.

* Standard - Taken from a ballast texture on the internet. 
* Good - Clean and sharp ballast, perfect for holding trains
* Medium - Grimy and blunter as ballast
* Poor Stained Ballest - With black stains from the thousands of diesel locomtives passing over, this is designed to go under track, making an interesting texture.
* Poor ballast - Grimy and past its best.
* Ballastless - stone with little clips for holding the track. 

Track decorations are used for making the track look nicer and more realistic

* Wire box - A small railside box containing wires
* Wires - Spinny little wires near the track

### Masts
Masts are very important to the signals. They hold up the signals, and are made of metal. There are various masts.

* Truss - Holds signals up with a collection of metal rods.
* Cylinder - A pole which you can attach various materials to.
* Phone - For calling the signalman
* Ladder - Used for climbing to level of signal
* Cylynder with walkway - Used for walking around the signal. Textured the same as factorybridges, allowing compatibility

### OLE
Overhead Line Equipment is very important to the modern railway line, without which, would be slow and dirty.

* Pillar - Standing proudly at the edges of the track are pillars, who hold up the equipment
* Top - This is at the very top of the pillar, and is where the holders start
* Holders - The backbone of the OLE arch, these hold up equipment, high above the tracks.
* Insulator - Stops the electricity going into the pillars and electrocuting the public
* Catenary - Holds up the wire

