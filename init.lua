dofile(minetest.get_modpath("britsignals") .. "/electricnodes.lua")
dofile(minetest.get_modpath("britsignals") .. "/masts.lua")
dofile(minetest.get_modpath("britsignals") .. "/ballast.lua")
dofile(minetest.get_modpath("britsignals") .. "/ole.lua")
dofile(minetest.get_modpath("britsignals") .. "/stationsigns.lua")
--dofile(minetest.get_modpath("britsignals") .. "/dynamic_signals.lua")

-- dofile(minetest.get_modpath("britsignals") .. "/stops.lua") -- Extremely verbose sum of a witch
dofile(minetest.get_modpath("britsignals") .. "/essentialstops.lua")