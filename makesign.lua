function makeSign(texture, description, size)
    minetest.register_node("britsignals:" .. texture, {  
        --tiles = {"transparent.png", "transparent.png", "transparent.png", "transparent.png", "transparent.png", texture},
        tiles = {texture .. ".png"},
	    light_source = 7, -- all must see!
	    
	    walkable = false,
	    description=description,
	    sunlight_propagates=true,
	    groups = {
		    cracky=3,
		    not_blocking_trains=1,
		    --save_in_at_nodedb=2,
	    },
	    
	    selection_box = {
            type = "wallmounted",
        },  
	    
	    visual_scale = size,
	        
	    drawtype = "signlike",
	    paramtype2 = "wallmounted",
    })
end

function makeStop(texture, description)
    minetest.register_node("britsignals:" .. texture, {  
        --tiles = {"transparent.png", "transparent.png", "transparent.png", "transparent.png", "transparent.png", texture},
        tiles = {texture .. ".billboard.png"},
	    light_source = 7, -- all must see!
	    
	    walkable = false,
	    description=description,
	    sunlight_propagates=true,
	    groups = {
		    cracky=3,
		    not_blocking_trains=1,
		    --save_in_at_nodedb=2,
	    },
	    
	    selection_box = {
            type = "wallmounted",
        },  
	    
	    visual_scale = 0.60,
	        
	    drawtype = "signlike",
	    paramtype2 = "wallmounted",
    })
end
