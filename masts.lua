minetest.register_node("britsignals:truss", {
    description = "Mast Truss",
    drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.375, -0.5, -0.5, -0.25, 0.5, -0.375}, -- NodeBox1
			{0.25, -0.5, -0.5, 0.375, 0.5, -0.375}, -- NodeBox2
			{-0.0625, -0.5, 0, 0.0625, 0.5, 0.125}, -- NodeBox3
			{-0.375, -0.375, -0.5, 0.375, -0.25, -0.375}, -- NodeBox4
			{0.25, -0.375, -0.375, 0.375, -0.25, -0.25}, -- NodeBox5
			{0.125, -0.375, -0.25, 0.25, -0.25, -0.125}, -- NodeBox6
			{0.0625, -0.375, -0.125, 0.125, -0.25, 0}, -- NodeBox7
			{-0.375, -0.3125, -0.375, -0.25, -0.25, -0.25}, -- NodeBox8
			{-0.25, -0.375, -0.25, -0.125, -0.25, -0.125}, -- NodeBox9
			{-0.125, -0.375, -0.125, -0.0625, -0.25, 0}, -- NodeBox10
			
			{-0.375, -0.3125, -0.5, 0.375, -0.25, -0.375}, -- NodeBox4
            {0.25, -0.3125, -0.375, 0.375, -0.25, -0.25}, -- NodeBox5
            {0.125, -0.3125, -0.25, 0.25, -0.25, -0.125}, -- NodeBox6
            {0.0625, -0.3125, -0.125, 0.125, -0.25, 0}, -- NodeBox7
            {-0.375, -0.3125, -0.375, -0.25, -0.25, -0.25}, -- NodeBox8
            {-0.25, -0.3125, -0.25, -0.125, -0.25, -0.125}, -- NodeBox9
            {-0.125, -0.3125, -0.125, -0.0625, -0.25, 0}, -- NodeBox10
		},
	},
    tiles = {"mast.png"}, -- Taken from Digtron
    paramtype2 = "facedir",
    paramtype = "light",
    sunlight_propagates = true,
    walkable = false,
    groups = {oddly_breakable_by_hand=1},
    --light_source = 7, -- Hopefully this reduces the black spot issue!
})

minetest.register_node("britsignals:cylinder_mast", {
	tiles = {"cylinder_mast.png"}, -- Taken from factory bridges
	description = "Cylinder Mast",
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.125, -0.5, -0.5, 0.1875, 0.5, -0.1875}, -- NodeBox1
		}
	},
    paramtype2 = "facedir",
    groups = {oddly_breakable_by_hand   = 3, snappy = 3},
})

minetest.register_craft({
    output = "britsignals:truss",
    recipe = {
        {"basic_materials:steel_bar","basic_materials:steel_bar",""},
        {"basic_marerials:steel_bar","","basic_materials:steel_bar"},
        {"basic_materials:steel_bar","basic_materials:steel_bar",""}
    }
})


