function makeOle(name, description, nodebox)
    minetest.register_node("britsignals:" .. name, {  
        --tiles = {"transparent.png", "transparent.png", "transparent.png", "transparent.png", "transparent.png", texture},
        tiles = {"steel.png"},
	    
	    walkable = true,
	    description=description,
	    sunlight_propagates=true,
	    groups = {
		    cracky=3,
		    not_blocking_trains=1,
		    --save_in_at_nodedb=2,
	    },
	    
	    paramtype2 = "facedir",
	    
	    drawtype = "nodebox",
	    paramtype = "light",
	    node_box = {
		type = "fixed",
		fixed = nodebox
	}
    })
end

makeOle("holder","OLE Holder", {{-0.5, 0.375, -0.0625, 0.5, 0.4375, 0.0625}})
makeOle("pillar","OLE Pillar", {{-0.1875, -0.5, -0.1875, 0.1875, 0.5, 0.1875}})
makeOle("top","OLE top", {{-0.1875, -0.5, -0.1875, 0.1875, 0.5, 0.1875}, -- NodeBox1
			{-0.5, 0.375, -0.125, 0.1875, 0.4375, 0.125}, -- NodeBox2
			{-0.3125, 0.25, -0.125, 0.25, 0.3125, 0.125}, -- NodeBox3
			{-0.4375, 0.3125, -0.125, -0.25, 0.375, 0.125}, -- NodeBox4
			{-0.5, 0.375, -0.125, -0.375, 0.4375, 0.125},})
			
makeOle("insulator", "OLE Insulator", {
			{-0.5, 0.375, -0.125, 0.5, 0.4375, 0.0625}, -- NodeBox1
			{-0.3125, 0.3125, -0.25, -0.1875, 0.5, 0.1875}, -- NodeBox2
			{-0.125, 0.3125, -0.25, 0, 0.5, 0.1875}, -- NodeBox3
			{0.0625, 0.3125, -0.25, 0.1875, 0.5, 0.1875}, -- NodeBox4
			{0.25, 0.3125, -0.25, 0.375, 0.5, 0.1875}, -- NodeBox5
		})

makeOle("catenary", "OLE Catenary", {{-0.5, 0.375, -0.0625, 0.5, 0.4375, 0.0625}, -- NodeBox1
			{0.3125, 0, -0.0625, 0.375, 0.5, 0.0625}, -- NodeBox2
			{-0.0625, 0.0625, -0.0625, 0.4375, 0.125, 0.0625}, -- NodeBox3
			{0, 0.0625, -0.5, 0.0625, 0.125, 0.5}, -- NodeBox4
			})
			
makeOle("wire","OLE Wire", {{0, 0.0625, -0.5, 0.0625, 0.125, 0.5}})
