local function place_degrotate(pos, placer, itemstack, pointed_thing)
	local yaw = placer:get_look_horizontal()
	local param = math.floor(yaw * 90 / math.pi + 0.5)
	local n = minetest.get_node(pos)
	n.param2 = param
	minetest.set_node(pos, n)
end

dofile(minetest.get_modpath("britsignals") .. "/makesign.lua")

makeSign("overhead_wires", "Overhead Wires Begin")
makeSign("electric_end", "Electronics End")
makeSign("dead_track", "Track Dead Entirely")
makeSign("flood", "Tracks Flooded")
makeSign("live_rails", "Live Rails")
makeSign("neutral_section", "Neutral Section Starts")
makeSign("neutral_section_warning", "Netural Section Warning")
makeSign("overhead_wires_alt", "Overhead Wires other")
makeSign("pantograph_down", "Pantograph Down")
makeSign("power_diesel", "Switch power to Diesel")
makeSign("power_electric", "Switch power to Electric")
