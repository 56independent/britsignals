-- GENERATED CODE
-- Node Box Editor, version 0.9.0
-- Namespace: test

minetest.register_node("test:node_1", {
	tiles = {
		"default_wood.png",
		"default_wood.png",
		"default_wood.png",
		"default_wood.png",
		"default_wood.png",
		"default_wood.png"
	},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, 0.375, -0.125, 0.5, 0.4375, 0.0625}, -- NodeBox1
			{-0.3125, 0.3125, -0.25, -0.1875, 0.5, 0.1875}, -- NodeBox2
			{-0.125, 0.3125, -0.25, 0, 0.5, 0.1875}, -- NodeBox3
			{0.0625, 0.3125, -0.25, 0.1875, 0.5, 0.1875}, -- NodeBox4
			{0.25, 0.3125, -0.25, 0.375, 0.5, 0.1875}, -- NodeBox5
		}
	}
})

