-- GENERATED CODE
-- Node Box Editor, version 0.9.0
-- Namespace: test

minetest.register_node("test:node_1", {
	tiles = {
		"default_wood.png",
		"default_wood.png",
		"default_wood.png",
		"default_wood.png",
		"default_wood.png",
		"default_wood.png"
	},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, 0.375, -0.0625, 0.5, 0.4375, 0.0625}, -- NodeBox1
			{0.3125, 0, -0.0625, 0.375, 0.5, 0.0625}, -- NodeBox2
			{-0.0625, 0.0625, -0.0625, 0.4375, 0.125, 0.0625}, -- NodeBox3
			{0, 0.0625, -0.5, 0.0625, 0.125, 0.5}, -- NodeBox4
		}
	}
})

