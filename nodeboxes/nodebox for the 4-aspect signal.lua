-- GENERATED CODE
-- Node Box Editor, version 0.9.0
-- Namespace: test

minetest.register_node("test:node_1", {
	tiles = {
		"default_wood.png",
		"default_wood.png",
		"default_obsidian.png",
		"default_obsidian.png",
		"default_obsidian.png",
		"4-aspect_signal.png"
	},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.1875, -0.375, -0.125, 0.25, 0.375, -0.0625}, -- NodeBox1
			{-0.125, -0.3125, -0.1875, 0.1875, 0.25, -0.125}, -- NodeBox2
			{-0.25, -0.5, -0.0625, 0.3125, 0.5, 0.375}, -- NodeBox3
		}
	}
})

