# Flow:
## Read template file
## Find >x<, which should only appear once
## Replace it with >[text]<
## Save in file of name [text].svg
## Call another script to compile svg into a png. 

import os

with open("stops.lua", "w") as file:
    file.write("dofile(minetest.get_modpath(\"britsignals\") .. \"/makesign.lua\")\n")
    file.close()

def compileSvg(file):
    file = "spam/" + file

    os.system("sh ./svg2bb.sh " + file) # Only works on linux machines.

    os.remove(file + ".svg") # Removes the svg source file, as it is unneeded
    os.remove(file + ".png") # Removes the png source file which is not billboard, as it is unneeded.


def makeLua(texture):
    import re 

    patternColour = "[a-z]+"
    patternNumber = "\d+?(_\d+)"
    
    matchColour = str(re.findall(patternColour, texture)[0])

    if re.findall(patternNumber, texture):
        matchNumber = str(re.findall(patternNumber, texture)[0])
    else:
        matchNumber = "thing?"

    if matchNumber != "thing?":
        description = "Stop " + matchColour + " of " + matchNumber + " cars"
    else:
        description = "Stop " + matchColour + " of " + matchNumber + " cars"


    with open("stops.lua", "a") as file: # append mode
        file.write("\nmakeStop(\"" + texture + "\", \"" + description + "\")")
        file.close()

def generate(template, text): # Where template is the colour name. 
    # Regex
    import re
    
    patternDashes = "-" # Dashes are not allowed in minetest?
    replaced = "_"
    textUnderscored = re.sub(patternDashes, replaced, text)
    
    # Stuff
    with open("template " + template + ".svg", 'r') as file:
        filedata = file.read()

    # Replace the target string
    filedata = filedata.replace(">x<", ">" + text + "<")

    newFilename = template + "_" + textUnderscored

    # Write the file out again
    with open("spam/" + newFilename + ".svg", "w") as file:
        file.write(filedata)

    compileSvg(newFilename)
    makeLua(newFilename)

def generateForAll(text):
    generate("white", text)
    generate("black", text)
    generate("blue", text)

# The following code contains repitition, which is known to the state of california to cause eye cancer

generateForAll("s")

generateForAll("1")
generateForAll("2") 
generateForAll("3")
generateForAll("4")
generateForAll("5")
generateForAll("6")
generateForAll("7")
generateForAll("8")
generateForAll("9")
generateForAll("10")
generateForAll("11")
generateForAll("12") 
generateForAll("13")
generateForAll("14")
generateForAll("15")
generateForAll("16")
generateForAll("17")
generateForAll("18")
generateForAll("19")
generateForAll("20")

generateForAll("1-1")
generateForAll("1-2") 
generateForAll("1-3")
generateForAll("1-4")
generateForAll("1-5")
generateForAll("1-6")
generateForAll("1-7")
generateForAll("1-8")
generateForAll("1-9")
generateForAll("1-10")

generateForAll("2-1")

generateForAll("3-1")
generateForAll("3-2")

generateForAll("4-1")
generateForAll("4-2") 
generateForAll("4-3")
generateForAll("4-5")

generateForAll("5-1")
generateForAll("5-2") 
generateForAll("5-3")
generateForAll("5-4")
generateForAll("5-6")

generateForAll("6-1")
generateForAll("6-2") 
generateForAll("6-3")
generateForAll("6-4")
generateForAll("6-5")

generateForAll("7-1")
generateForAll("7-2") 
generateForAll("7-3")
generateForAll("7-4")
generateForAll("7-5")
generateForAll("7-6")

generateForAll("8-1")
generateForAll("8-2") 
generateForAll("8-3")
generateForAll("8-4")
generateForAll("8-5")
generateForAll("8-6")
generateForAll("8-7")

generateForAll("9-1")
generateForAll("9-2") 
generateForAll("9-3")
generateForAll("9-4")
generateForAll("9-5")
generateForAll("9-6")
generateForAll("9-7")
generateForAll("9-8")

generateForAll("10-1")
generateForAll("10-2") 
generateForAll("10-3")
generateForAll("10-4")
generateForAll("10-5")
generateForAll("10-6")
generateForAll("10-7")
generateForAll("10-8")
generateForAll("10-9")
