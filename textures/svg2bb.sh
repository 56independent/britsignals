#!/bin/bash
# svg2bb.sh
# Prepare an SVG image for placing on a minetest billboard
# Requires inkscape and imagemagick on the path
# Invoke without any file extension, e.g. RegionalRailMap
filename=$1

#url=$(grep -oE filesrc.html -m1 -e '/mediawiki/images/[a-f0-9]/[a-f0-9]{2}/'${filename}'\.svg')
#url=https://wiki.linux-forks.de${url}
#wget $url -O ${filename}.svg

#inkscape ${filename}.svg --export-area-page -e ${filename}.png
inkscape --without-gui ${filename}.svg -w 200 -o ${filename}.png


size=$(identify -format "%wx%h" ${filename}.png | sed 's,x,\n,' - | sort -n | tail -n1)
composite -compose src-over ${filename}.png ${filename}.png -background white -alpha remove ${filename}.billboard.png
mogrify -background transparent -gravity center -extent ${size}x${size} ${filename}.billboard.png -alpha activate ${filename}.billboard.png
