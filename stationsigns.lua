-- Adds signs used around pedestrians

dofile(minetest.get_modpath("britsignals") .. "/makesign.lua")

makeSign("trespassing_pound_billboard", "Tresspassing Warning in Pounds", 0.1)
makeSign("trespassing_mg_billboard", "Tresspassing Warning in Minegeld", 0.1)
makeSign("horn_billboard", "Blow Horn Sign", 0.1)
makeSign("turbulence_billboard", "Train Turbulence Warning", 0.1)
makeSign("crossing_billboard", "Warning of Risk of Crossings", 1.25)
makeSign("kill_", "Trains Kill", 1.50)
makeSign("kill_simple_", "Trains Kill, Simple version, small", 1.25)
makeSign("kill_small", "Trains Kill, small", 1)
makeSign("kill_simple_small", "Trains Kill, Simple version", 0.9)
makeSign("height_", "Height Warning", 1)
makeSign("queues_station_", "Queue warning because of station", 1.75)
makeSign("queues_ghost_", "Queue warning because of ghost trains", 1.75)
makeSign("silent_", "Silent trains warning", 0.9)