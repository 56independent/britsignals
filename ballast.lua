function make_ballast(name, description)
    minetest.register_node("britsignals:" .. name,{
        tiles = {name .. ".png"},
        sunlight_propagates = false,
        groups = {crumbly = 3, snappy = 3   },
        description = description,
        
        paramtype2 = "facedir",
    })
end

make_ballast("ballast","Stabdard Ballast")
make_ballast("good_ballast", "Good Ballast")
make_ballast("medium_ballast", "Medium Ballast")
make_ballast("poor_ballast", "Poor Ballast")
make_ballast("poor_ballast_stained", "Stained Poor Ballast")
make_ballast("ballastless", "Ballestless Track Surface")

minetest.register_node("britsignals:wirebox", {
    drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.1875, -0.5, -0.5, -0.125, -0.3125, 0.5}, -- NodeBox1
			{0.1875, -0.5, -0.5, 0.25, -0.3125, 0.5}, -- NodeBox2
			{-0.1875, -0.375, -0.5, 0.25, -0.3125, 0.5}, -- NodeBox3
		}
	},
	paramtype2 = "facedir",
	tiles = {"floor.png"},
    groups = {crumbly = 3, snappy = 3},
    description = "Wirebox",
})

minetest.register_node("britsignals:wirebox_end", {
    drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.1875, -0.5, -0.5, -0.125, -0.3125, 0.5}, -- NodeBox1
			{0.1875, -0.5, -0.5, 0.25, -0.3125, 0.5}, -- NodeBox2
			{-0.1875, -0.375, -0.5, 0.25, -0.3125, 0.5}, -- NodeBox3
			{-0.1875, -0.5, -0.5, 0.25, -0.3125, -0.4375}, -- NodeBox4
		}
	},
	paramtype2 = "facedir",
	tiles = {"floor.png"},
    groups = {crumbly = 3, snappy = 3},
    description = "Wirebox Ends",
})
